{-
real    9m22.257s
user    9m14.573s
sys 0m5.703s
-}

import Control.Monad.State
import Data.Char
import System.Environment  
import System.IO
import Text.Parsec
import Text.Parsec.Combinator
import Text.Parsec.Char
import Text.Parsec.String

data Instruction = InsNext | InsPrevious
                 | InsIncrement | InsDecrement
                 | InsOutput | InsInput
                 | InsLoop [Instruction]
                 deriving (Show)

data Tape = Tape [Int] Int [Int]

emptyTape :: Tape
emptyTape = Tape (repeat 0) 0 (repeat 0)

comment :: Parser Char
comment = noneOf "><+-.,[]"

program :: Parser [Instruction]
program = do
    skipMany comment
    instruction `sepEndBy` (many comment)

instruction :: Parser Instruction
instruction = convert <$> noneOf "[]" <|> loop
    where
        convert '>' = InsNext
        convert '<' = InsPrevious
        convert '+' = InsIncrement
        convert '-' = InsDecrement
        convert '.' = InsOutput
        convert ',' = InsInput

loop :: Parser Instruction
loop = InsLoop <$> between (char '[') (char ']') program

run :: [Instruction] -> IO ()
run xs = evalStateT (mapM_ eval xs) emptyTape

eval :: Instruction -> StateT Tape IO ()
eval InsNext = modify (\(Tape ls x (r:rs)) -> Tape (x:ls) r rs)
eval InsPrevious = modify (\(Tape (l:ls) x rs) -> Tape ls l (x:rs))
eval InsIncrement = modify (\(Tape ls x rs) -> Tape ls (x+1) rs)
eval InsDecrement = modify (\(Tape ls x rs) -> Tape ls (x-1) rs)
eval InsOutput = get >>= (\(Tape ls x rs) -> liftIO . putChar . chr $ x)
eval InsInput = do
    input <- liftIO $ fromEnum <$> getChar
    modify (\(Tape ls x rs) -> Tape ls input rs)
eval loop@(InsLoop xs) = get >>= (\(Tape ls x rs) -> executeLoop x)
    where
        executeLoop x
            | x == 0 = return ()
            | otherwise = mapM_ eval (xs ++ [loop])

main = do
    args <- getArgs
    content <- readFile $ head args
    case parse program "Program" content of
        Left x -> putStrLn $ "Syntax error: " ++ show x
        Right x -> run x
